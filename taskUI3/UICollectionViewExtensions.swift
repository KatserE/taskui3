//
//  UICollectionViewExtensions.swift
//  taskUI3
//
//  Created by Евгений Кацер on 16.05.2023.
//

import UIKit

extension UICollectionView {
    func register(cell: CustomCell.Type) {
        register(cell.getNib(), forCellWithReuseIdentifier: cell.getID())
    }
}
