//
//  WayForPhotoView.swift
//  taskUI3
//
//  Created by Евгений Кацер on 18.05.2023.
//

import UIKit

class WayForPhotoView: UIViewController {
    @IBOutlet private weak var cameraButton: UIButton!
    @IBOutlet private weak var galleryButton: UIButton!

    weak var delegate: ChoosePhotoWayDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    private func closeWindow() {
        dismiss(animated: true)
    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        closeWindow()
    }

    @IBAction func someButtonPressed(_ sender: Any) {
        closeWindow()

        switch sender as! UIButton {
        case cameraButton:
            delegate.choosenCamera()
        case galleryButton:
            delegate.choosenGallery()
        default:
            break
        }
    }
}
