//
//  ImageCollectionViewCell.swift
//  taskUI3
//
//  Created by Евгений Кацер on 09.05.2023.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell, CustomCell {
    @IBOutlet private weak var image: UIImageView!
    
    func configure(image: UIImage) {
        self.image.image = image
    }
}
