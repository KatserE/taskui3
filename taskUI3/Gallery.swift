//
//  Gallery.swift
//  taskUI3
//
//  Created by Евгений Кацер on 17.05.2023.
//

import UIKit

struct Gallery {
    private var photos = [UIImage]()
    var count: Int { photos.count }
    
    subscript(index: Int) -> UIImage {
        get {
            photos[index]
        }
        set {
            photos[index] = newValue
        }
    }
    
    mutating func addImage(_ image: UIImage) {
        photos.append(image)
    }
}
