//
//  ViewController.swift
//  taskUI3
//
//  Created by Евгений Кацер on 09.05.2023.
//

import UIKit
import PhotosUI

protocol ChoosePhotoWayDelegate : AnyObject {
    func choosenCamera()
    func choosenGallery()
}

class ViewController: UIViewController,
                      UICollectionViewDelegate, UICollectionViewDataSource,
                      UICollectionViewDelegateFlowLayout,
                      UINavigationControllerDelegate, UIImagePickerControllerDelegate,
                      PHPickerViewControllerDelegate,
                      ChoosePhotoWayDelegate {
    private static let modalID = "modelViewID"
    
    @IBOutlet weak var galleryView: UICollectionView!
    private var galleryModel = Gallery()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        galleryView.delegate = self
        galleryView.dataSource = self
        
        galleryView.register(cell: ImageCollectionViewCell.self)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        galleryModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ImageCollectionViewCell.getID(),
            for: indexPath
        ) as! ImageCollectionViewCell
        
        cell.configure(image: galleryModel[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: view.frame.width/3 - 3, height: view.frame.height/3 - 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
         UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1 )
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        
        addImage(image)
    }
    
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)

        let itemProvider = results.first?.itemProvider
        if let itemProvider, itemProvider.canLoadObject(ofClass: UIImage.self) {
            itemProvider.loadObject(ofClass: UIImage.self) { [weak self] image, error in
                DispatchQueue.main.async {
                    if let image = image as? UIImage {
                        self?.addImage(image)
                    }
                }
            }
        }
    }
    
    func choosenCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true)
    }
    
    func choosenGallery() {
        var config = PHPickerConfiguration()
        config.selectionLimit = 1
        config.filter = .images
        
        let picker = PHPickerViewController(configuration: config)
        picker.delegate = self
    
        present(picker, animated: true)
    }
    
    @IBAction func addImageButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: ViewController.modalID, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewController.modalID {
            let destination = segue.destination as! WayForPhotoView
            destination.delegate = self
        }
    }
    
    private func addImage(_ image: UIImage) {
        galleryModel.addImage(image)
        galleryView.reloadData()
    }
}
